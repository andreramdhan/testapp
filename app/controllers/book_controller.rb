class BookController < ApplicationController
	def list
    @book_pages, @books = paginate :books, :per_page => 10
  end

  def show
    @book = Book.find(params[:id])
  end

  def search
    @book = Book.find_by_title(params[:title])
    if @book
      redirect_to :action => 'show', :id => @book.id
    else    
      flash[:error] = 'No such book available'
      redirect_to :action => 'list'
    end
  end
end
