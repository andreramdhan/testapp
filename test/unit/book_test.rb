
require 'test_helper'

class BookTest < ActiveSupport::TestCase
  
  fixtures :books

  def test_book

    perl_book = Book.new :title => books(:perl_cb).title, 
                         :price => books(:perl_cb).price,
                         :description => books(:perl_cb).description,
                         :created_at => books(:perl_cb).created_at

    assert perl_book.save

    perl_book_copy = Book.find(perl_book.id)

    assert_equal perl_book.title, perl_book_copy.title

    perl_book.title = "Ruby Tutorial"

    assert perl_book.save
    assert perl_book.destroy
  end
  
end
